I like seeing movies at the [Museum of the Moving
Image](http://www.movingimage.us/). But it's a pain to individually
access each event's `.ics` (iCalendar) file and import it into my
electronic calendar -- and the museum's `.ics` files often include a
misleading `DTEND` that implies the event has a duration of 0 seconds.
This script takes a plain text file of URLs separated by newlines (see
`movie-urls-sample-file.txt` for an example), downloads iCalendar
files from the MoMI site, fixes their event end times, and creates a
new unified `.ics` file ready for import into a calendar.

This is somewhat unmaintained; [![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

TODO:

* Fix timezone-handling bug; right now the start and end times are messed up
  in a way I suspect involves UTC handling (not quite always but fairly
  consistently 5 hours early, in NYC).
* If MoMI's calendar event (`.ics` file) has a nonzero event duration
  but is _wrong_ according to the duration on the human-readable
  webpage
  ([example](http://www.movingimage.us/visit/calendar/2019/09/06/detail/triple-threat)),
  use the longer duration.

Disclaimers:

* It can be a bit slow as the number of URLs adds up -- it took maybe
  5 minutes to process about 31 events. I oughta profile it and speed
  it up. But I usually only need to do this about six times a year.
* This script is not careful, and will overwrite a previously created
  `.ics` file at the same address (in case you're running it twice in
  one day). It has no tests and approximately no error-checking.
* Absolutely not an official project of the Museum of the Moving Image.
  (I asked them to fix the `DTEND` issue on their end and now some, but not
  all, of their iCalendar files have proper durations. So I'm doing this.)
* [![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

Much thanks to the programming ecology that helped me build this,
especially the people who made http://www.regexr.com/ , Beautiful
Soup, requests, and ics.py, and the many who have written excellent
documentation on Python's standard library.

[Blog post announcement, with screenshot.](https://www.harihareswara.net/sumana/2016/09/26/0)
