#!/usr/bin/python3
"""Usage: python create-fixed-ics.py file-of-URLs

Using a plain text file of URLs separated by newlines, download ical
files from the Museum of the Moving Image site, fix their DTEND times,
and create a new .ics file ready for import into a calendar.

"""

# virtualenv name: ical3
# pip3 install html5lib and upgrade setuptools past 18.5


import ics
import datetime
import requests
import sys
import bs4
import re


def getURLs():
    """ Use sys.argv to get filename
    Get list of Museum URLs out of that file
    Return a list of strings
    """
    filename = sys.argv[1]
    with open(filename) as f:
        urllist = list(f)
    return urllist

def getCalendar(url):
    """ Given a Museum movie URL, use Requests to get its .ics file
    [replace "detail" with "ical"]
    Return a Calendar object.
    """
    icsurl = url.replace("detail", "ical")
    icsfile = requests.get(icsurl)
    moviecal = ics.Calendar(icsfile.text)
    for item in moviecal.events:
        item.url = url
    return moviecal

def concatenateCalendars(calendarlist):
    """ Given a list of calendars,
    move all the events into a single calendar
    Return that calendar
    """
    concatenatedcalendar = ics.Calendar()
    for cal in calendarlist:
        for event in cal.events:
            concatenatedcalendar.events.add(event)
    return concatenatedcalendar

def getMovieDescription(url):
    """ Given a detail URL,
    use Requests to get the content of that page.
    Use Beautiful Soup to extract body of the movie description.
    Return the description text.
    """
    content = requests.get(url).content
    soup = bs4.BeautifulSoup(content, "html5lib")
    descriptionpara = soup.find("div", attrs={"id": "event-description"}).get_text()
    return descriptionpara

def determineLengths(descriptiontext):
    """
    Given movie description text,
    Use a regex to find a "program duration" if it exists.
    (Much thanks to http://www.regexr.com/ .)
    If it doesn't: use a regex to find places containing an int before "mins.".
    If "intermission" is mentioned, append "15".
    Return a list of those ints.
    """
    progduration = re.compile("Program duration:\s[0-9]+\smin[s]*[.|(\s\w)]")
    overallduration = re.findall(progduration, descriptiontext)
    if overallduration != []:
        durationlist = overallduration # most reliable
    else:
        timepattern = re.compile("[0-9]+\smin[s]*[.|(\s\w)]") # match "116 min[s]"
        durationlist = re.findall(timepattern, descriptiontext)
        intermission = re.findall(re.compile("intermission"), descriptiontext)
        if intermission != []:
            for elem in intermission:
                durationlist.append("15 mins.")
    intpattern = re.compile("[0-9]+") # get the numbers of minutes
    minutelist = [int(re.findall(intpattern, x)[0]) for x in durationlist]
    return minutelist

def createEndTime(event, minutelist):
    """ Given an event and a list of minute lengths, use
    datetime.timedelta(minutes=)
    to change event.end to the correct time.
    If there's more than one int, note in event description that this is suspect.
    Return the event.
    """
    if len(minutelist) == 1:
        event.end = event.end+datetime.timedelta(minutes=minutelist[0])
        return event
    else:
        for item in minutelist:
            event.end = event.end+datetime.timedelta(minutes=item)
        event.description = "MULTIPLE TIME DURATIONS - LENGTH MAY BE WRONG " + event.description
        return event

def main():
    """Read list of detail URLs from file.
    For each of those URLs, get the ics file and turn it into a Calendar object.
    Concatenate together the events from those calendars into one.
    For each event, check whether the start & end times are the same.
    (If they are, figure out the real end time, and then fix the event's end time.)
    Write out a .ics file at the end and tell the user where it is.
    """
    urllist = getURLs()
    calendarlist = []
    for URL in urllist:
        calendarlist.append(getCalendar(URL))
    unifiedcalendar = concatenateCalendars(calendarlist)
    for event in unifiedcalendar.events:
        if event.begin == event.end:
            moviedescription = getMovieDescription(event.url)
            minutelist = determineLengths(moviedescription)
            event = createEndTime(event, minutelist)
        print(event.name)
    filename = "MoMI-movies-chosen-" + datetime.date.isoformat(datetime.date.today()) + ".ics"
    with open(filename, "w") as f:
        f.writelines(unifiedcalendar)
    print("Calendar ready for importing: " + filename)

if __name__=="__main__":
    main()
